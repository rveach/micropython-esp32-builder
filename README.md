# micropython-esp32-builder

I'm going to use this image to auto-build micropython projects for the ESP32.
This image was intended to run in CI/CD environments, but there's no reason you couldn't run it from the cli.

It compiles micropython and mpy-cross from source. See the .gitlab-ci.yml test job to see python source compiled into bytecode and native.

Disclaimer: I'm new at using MicroPython and ESP32's, so I may have to tweak this as I go.

Image: registry.gitlab.com/rveach/micropython-esp32-builder:latest
