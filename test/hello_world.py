from machine import Pin
from time import sleep

led = Pin(16, Pin.OUT)

led_value = 0

while True:
    led.value=led_value
    led_value = 0 if led_value==1 else 1
    sleep(1)
