FROM python:3

RUN apt-get update && apt-get install -y \
        build-essential \
        libreadline-dev \
        libffi-dev \
        git \
        pkg-config \
        gcc-arm-none-eabi \
        libnewlib-arm-none-eabi \
        && rm -rf /var/lib/apt/lists/*

WORKDIR /src
RUN git clone --recurse-submodules https://github.com/micropython/micropython.git

WORKDIR /src/micropython/mpy-cross
RUN make

WORKDIR /src/micropython/ports/unix
RUN make axtls
RUN make

ENV PATH="/src/micropython/mpy-cross:/src/micropython/ports/unix:${PATH}"

WORKDIR /project
